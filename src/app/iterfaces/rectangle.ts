export interface Rectangle {
  height: number;
  width: number;
}
