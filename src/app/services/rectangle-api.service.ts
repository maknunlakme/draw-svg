import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Rectangle} from '../iterfaces/rectangle';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {HeaderService} from './header.service';

@Injectable({
  providedIn: 'root'
})
export class RectangleApiService {

  constructor(
    private http: HttpClient,
    private headerService: HeaderService
  ) {
  }

  getRectangleValue(): Observable<Rectangle> {
    const headers = this.headerService.newHeader();
    return this.http.get<Rectangle>(environment.apiUrl + '/figure', {headers});
  }

  postRectangleValue(body: Rectangle): Observable<any> {
    const headers = this.headerService.newHeader();
    return this.http.post(environment.apiUrl + '/figure', body, {headers, responseType: 'text'});
  }
}
