import {Component, ElementRef, EventEmitter, HostListener, Input, Output, ViewChild} from '@angular/core';
import {Rectangle} from '../iterfaces/rectangle';

@Component({
  selector: 'app-rectangle',
  templateUrl: './rectangle.component.html',
  styleUrls: ['./rectangle.component.scss']
})
export class RectangleComponent {
  @ViewChild('box') public box: ElementRef;
  @Input() rectangle: Rectangle;
  @Output() perimeterEvent = new EventEmitter<Rectangle>();
  status = 0;
  mouse: { x: number, y: number };

  @HostListener('window:mousemove', ['$event'])
  onMouseMove(event: MouseEvent): void {
    this.mouse = {x: event.clientX, y: event.clientY};
    if (this.status === 1) {
      this.resize();
    }
  }

  constructor() {
  }

  setStatus(value: number): void {
    if (this.status === 1 && value === 0) {
      this.perimeterEvent.emit(this.rectangle);
    }
    this.status = value;
  }

  resize(): void {
    this.rectangle.width = Math.round(Math.abs(this.mouse.x - this.box.nativeElement.getBoundingClientRect().left));
    this.rectangle.height = Math.round(Math.abs(this.mouse.y - this.box.nativeElement.getBoundingClientRect().top));
  }
}
