import {Component, OnDestroy, OnInit} from '@angular/core';
import {RectangleApiService} from './services/rectangle-api.service';
import {Rectangle} from './iterfaces/rectangle';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'draw-svg';
  rectangle: Rectangle;
  getRectangleSubscription: Subscription;
  postRectangleSubscription: Subscription;

  constructor(private rectangleApiService: RectangleApiService) {
  }

  ngOnInit(): void {
    this.getRectangleSubscription = this.rectangleApiService
      .getRectangleValue()
      .subscribe((data) => {
        this.rectangle = data;
      });
  }

  getPerimeter(event: Rectangle): void {
    this.postRectangleSubscription = this.rectangleApiService
      .postRectangleValue(event)
      .subscribe(() => {
      });
  }

  ngOnDestroy(): void {
    this.getRectangleSubscription.unsubscribe();
    this.postRectangleSubscription.unsubscribe();
  }
}
